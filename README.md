# Create VM in Digital Ocean using Terraform 


Examples how to use Terraform to deploy virtual hosts

## Requirements:

* [Terraform](https://www.terraform.io/downloads.html)
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)



### First steps



## Author

* **Ivan Kostin** - [Skensell](https://github.com/skensell201)

## Materials for creating instructions
* [DO API Documentation](https://developers.digitalocean.com/documentation/v2/)
* Provider Terraform [Digital Ocean](https://www.terraform.io/docs/providers/do/index.html)



## Feedback

* You can always contact [me](mailto:skense1@yandex.ru)