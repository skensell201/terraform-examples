# Secret variables - defined in terraform.tfvars
variable "vsphere_user" { description = "Secret variable vSphere user"}
variable "vsphere_password" { description = "Secret variable vSphere password" }
variable "vsphere_server" { description = "Secret variable vSphere server" }
# vSphere variables
variable "datacenter" { default = "Datacenter1" }
variable "name_datastore" { default = "datastore1" }
variable "pool_name" { default = "pool1" }
variable "network_name" { default = "network_name" }
# VM variables
variable "vm_name" { default = "terraform-vm" }
variable "vm_domain" { default = "test.io" }
variable "number_cpus" { default = 2 }
variable "memory_size" { default = 4096 }
variable "folder_vm" { default = "MAINFOLDER/SECONDFOLDER" }
variable "size_disk" { default = 25 }
variable "os_type" { default = "centos7_64Guest" }
variable "template_name" { default = "CentOS7-2009tmplt" }
variable "firmware_type" { default = "efi" }
variable "ipv4_addr" { default = "10.10.115.10" }
variable "ipv4_gtw" { default = "10.10.115.1" }
variable "dns_servers" { default = ["8.8.8.8"] }

