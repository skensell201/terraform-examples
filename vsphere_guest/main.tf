# Section of providers
terraform {
  required_providers {
    vsphere   = {
      source  = "registry.terraform.io/hashicorp/vsphere"
      version = "~> 1.15.0"
    }
  }
}

provider "vsphere" {
  user                 = var.vsphere_user
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_server
  allow_unverified_ssl = true
}

# Section of resources
resource "vsphere_virtual_machine" "vm" {
  name             = var.vm_name
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id     = data.vsphere_datastore.datastore.id
  num_cpus         = var.number_cpus
  memory           = var.memory_size
  folder           = var.folder_vm
  guest_id         = var.os_type
  firmware         = var.firmware_type

  cpu_performance_counters_enabled = true
  cpu_hot_add_enabled              = true
  memory_reservation               = 0
  wait_for_guest_net_timeout       = 0
  wait_for_guest_ip_timeout        = 0

    clone {
        template_uuid = data.vsphere_virtual_machine.template.id
        
        customize {

            linux_options {
                host_name = var.vm_name
                domain    = var.vm_domain
            }

            network_interface {
                ipv4_address = var.ipv4_addr
                ipv4_netmask = 24
            }

            ipv4_gateway    = var.ipv4_gtw
            dns_server_list = var.dns_servers
        }
    }

    network_interface {
        network_id = data.vsphere_network.network.id
    }

    disk {
        label = "disk0"
        size  = var.size_disk
    }
    
}