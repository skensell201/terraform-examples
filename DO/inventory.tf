data "template_file" "inventory" {
  template = "${file("inventory.tpl")}"
  vars = {
    vps1 = digitalocean_droplet.vps[0].ipv4_address
    vps2 = digitalocean_droplet.vps[1].ipv4_address
    vps3 = digitalocean_droplet.vps[2].ipv4_address
  }
}

resource "local_file" "file" {
  content  = "${data.template_file.inventory.rendered}"
  filename = "../ansible/inventory"
}
