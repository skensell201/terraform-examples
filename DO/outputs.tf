#####     SECTION - OUTPUTS     #####

output "vm_information" {
  value = "${formatlist(
    "%s = %s",
    (digitalocean_droplet.vps.*.name),
    (digitalocean_droplet.vps.*.ipv4_address)
  )}"
}
#####     END SECTION - OUTPUTS     #####