[all]
${vps1}
${vps2}
${vps3}

[manager]
${vps1}

[workers]
${vps2}
${vps3}

[all:vars]
ansible_ssh_private_key_file = ~/.ssh/id_rsa
ansible_ssh_user = root
