variable "DO_TOKEN" {
  description = "The value is taken from the environment variable TF_VAR_DO_TOKEN"
}

variable "DO_NAME_SSH" {
  description = "The value is taken from the environment variable TF_VAR_DO_NAME_SSH"
}

variable "DO_NAME_SSH_ANOTHER" {
  description = "The value is taken from the environment variable TF_VAR_DO_NAME_SSH_ANOTHER"
}

variable "DO_EMAIL" {
  description = "The value is taken from the environment variable TF_VAR_DO_EMAIL. Must be kind name_at_example_com -> (name@example.com)"
}
variable "servers" {
  default = ["vps1", "vps2", "vps3"]
}
