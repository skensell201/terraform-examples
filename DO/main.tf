#####     SECTION - PROVIDERS      #####
provider "digitalocean" {
  token = var.DO_TOKEN
}
#####     END SECTION - PROVIDERS     #####

#####     SECTION - DATA WHICH YOU WANT TO GET     #####
data "digitalocean_ssh_key" "ssh_key" {
  name = var.DO_NAME_SSH
}

data "digitalocean_ssh_key" "another_ssh_key" {
  name = var.DO_NAME_SSH_ANOTHER
}
#####     END SECTION - DATA WHICH YOU WANT TO GET     #####

#####     SECTION - RESOURCES     #####

resource "digitalocean_tag" "tag_email" {
  name = var.DO_EMAIL
}

resource "digitalocean_droplet" "vps" {
  count    = length(var.servers)
  name     = var.servers[count.index]
  image    = "ubuntu-18-04-x64"
  region   = "nyc1"
  size     = "s-1vcpu-2gb"
  ssh_keys = [data.digitalocean_ssh_key.ssh_key.fingerprint, data.digitalocean_ssh_key.another_ssh_key.id]
  tags     = [digitalocean_tag.tag_email.id]

  connection {
    host        = self.ipv4_address
    type        = "ssh"
    user        = "root"
    private_key = file("~/.ssh/id_rsa")
  }
}

resource "null_resource" "ansible_provisioner" {
  count = 1
  depends_on = [digitalocean_droplet.vps]

  provisioner "local-exec" {
    command = "ansible-playbook -i ../ansible/inventory ../ansible/docker.yml"
  }
}

/* If you need add new ssh key
      Then "ssh_key" in resource "digitalocean_droplet" must contain [digitalocean_ssh_key.ssh_key.fingerprint/id/name]

resource "digitalocean_ssh_key" "my_public_key" {
  name       = var.DO_NAME_SSH
  public_key = file("~/.ssh/id_rsa.pub")
}
*/

#####     END SECTION - RESOURCES     #####


